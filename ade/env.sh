#!/usr/bin/env bash

source "/opt/ros/$ROS_DISTRO/setup.bash"
source "/opt/ros/extra/install/setup.bash"
source /usr/share/colcon_argcomplete/hook/colcon-argcomplete.bash

for x in /opt/*; do
    if [[ -e "$x/.env.sh" ]]; then
	source "$x/.env.sh"
    fi
done

cd
