# Exercise 1: Sensing and Acting

In this exercise we will create a ROS 2 package to control the BoldBot
humanoid robot. We will use the ROS Client Library for Python
(`rclpy`) to develop a node that subscribes to and publishes on the
required topics. We have provided some boiler plate to get you
started. First thing, copy this over to the source folder of the
workspace you will work in:

    # ensure you’re in the ADE home directory
    cd
    mkdir -p src
    cp -r ros2_workshop/exercise_1/ros2_sense_act src/
    colcon build --symlink-install --packages-up-to ros2_sense_act
    source install/local_setup.bash

Because this build command installs the package by creating symbolic
links to the source files you will be working on, you will not have to
run it again (unless you create some new nodes of your own).

Open the file `src/ros2_sense_act/sense_act.py` in an
editor; this is where our node is defined and where you will implement
its logic.

## Step 1: Subscribing to a topic

As we have seen before, the simulated robot publishes sensor messages
onto several topics. To create responsive behavior, our node needs to
*subscribe* to these topics to receive this sensor data. This is done
with the `create_subscription` method of a node; find it in
`init_subscription`. To subscribe properly, you have to change the
method call to correctly provide 4 things:

1. The message type that is expected on the topic. We will look at
   joint states first, find the message type for those in the imports
   at the top of the file, and set it as the first argument.
2. The name of the topic to subscribe to. We have seen that in the
   previous exercise; you can also run `ros2 topic list` while the
   simulation is running to find it.
3. A callback function that takes a message as its single argument. At
   the moment the provided callback is a lambda function that doesn't
   do anything. For now just output the full message by calling
   `self.get_logger().info(str(msg))` instead of `None`.
4. A 'quality of service' profile. We will go into that deeper in the
   next exercise, we just use one of the default built in profiles
   here.
        
Make sure these things are provided, then uncomment the call to
`init_subscription` in the `__init__` method and save. If done
correctly, you can now run your node and see a lot of joint
information whizz by. With the simulation running, run the following
in another ADE terminal:

    ros2 run ros2_sense_act sense_act

You will see a lot of information go by. Stop the node (Ctrl+c) and
have a closer look at the data. You can compare it to the output of:

    ros2 interface show sensor_msgs/msg/JointState

## Step 2: Publishing to a topic

Now that we have some sensing, let's do some acting as well! This is
done by *publishing* to a topic that a controller is listening on. For
this you have to create a publisher, which is done in a very similar
way to setting up a subscription, using the node's `create_publisher`
method; find it in `init_publisher`. The arguments to supply are
almost the same as before:

1. The message type to be sent on the topic. We will be sending joint
   commands here, so use the proper type for that.
2. The topic name. Again, you have seen this before, and you can find
   it with `ros2 topic list`.
3. A 'quality of service' profile. Again, we will use a default.

Make the relevant changes and uncomment the call to `init_publisher`
in `__init__` and save. If you would rerun the node now you wouldn't
see a difference. In `init_publisher` you see code to create and
publish a message, but at the moment this message is empty, so it has
no effect.

To send a proper joints command, you have to provide the following:

1. `msg.name`: a list of joint names to control. We just want to
   control the head pan (left-right looking angle) for now, so add the
   following before the message is published:

        msg.name = ["head-pan"]

   You can see all the joint names in the joint state information
   logged by the subscription.
2. `msg.position`: a list of target angles in radians for the named
   joints. For instance, to look 1 radian to the side:
   
        msg.position = [1.0]
    
Try some different angles, save and rerun the node, and you should see
the robot look at the angle you specified in Gazebo. Before going on,
set the angle to 0.0 to have the robot start with looking straight.

## Step 3: Act based on sensor input

Closed-loop behavior is not very interesting, a robot should act based
on its sensor input. Firstly, replace the lambda that is logging the
joint states to a reference of the `handle_msg` method, so we can use
a more complex callback function:

    self.sub = self.create_subscription(
        ...,
        ...,
        self.handle_msg,
        ....,
    )

Then have a look at the implementation of `handle_msg` where we will
implement a very sophisticated behavior: we want to have the head
*tilt* syncronized with the head *pan*. In other words, whenever the
head pan is changed, we want the robot to sense this and set the tilt
angle to the same value. This will make the robot look down when he
looks left, and up when he looks right.

To do that, first get the current pan angle from the joint states message:

    current_head_pan = msg.position[6]

The index into the position array can be found by counting through the
names in the message; "head-pan" corresponds to 6.

Next, fill in the target joint and angle as before. This time we want
to control `"head-tilt"` and set its target angle to the same as the
current pan angle. Save and rerun the node.

To see the sense-act loop in action, try setting the head pan to
different angles with the ROS 2 CLI in a new ADE terminal:

    ros2 topic pub --once /cm730/joint_commands mx_joint_controller_msgs/msg/JointCommand '{name: ["head-pan"], position: [1.0]}'

You should see that the robot's head tilt angle will always follow the
pan angle. We have a full sense-act loop!

# Step 4: Timed action

Besides action in response to incoming messages it is also possible to
act based on time. In ROS 2 you can use timers to perform some logic
at fixed intervals. We will use that now to create a smooth
look-around behavior.

Setting up a timer is very similar to setting up a subscription, by
specifying a callback function to be called, but instead of it being
triggered by new messages it will be triggered continuously after some
period. Find the call to `create_timer` which sets this up and
uncomment it (as well as the two lines before that that initialise
values used for the behavior). You see that the timer will trigger
every 0.02 seconds, i.e. with 50 Hz.

We have already fully specified the implementation of `handle_tick`,
because it is mostly more of the same: we determine a target angle for
a joint, construct a corresponding message and publish it. The
interesting thing here is how we determine the target position: we use
the node's `self.get_clock()` method to get the current time, and we
use a sinusoid to calculate a smoothly changing target angle.

Save and rerun and you will see the robot look left and right, *and*
up and down thanks to the link between pan and tilt angle that we have
set up.

# Step 5: Control behavior parameters

If you want to change some of the properties of the robot's behavior,
you currently have to stop it, edit the code, and rerun it. ROS 2
allows you to do that much more nicely while the node is running
through *parameters*. To use these, you have to declare them when your
node is created. With that, parameter values could be set on startup
of the node. You can further also register a callback to respond to
requests to change the parameter *during* runtime.

Find the corresponding two lines in the `__init__` method and
uncomment them. They show that a parameter `"period"` is declared,
with a default value of 4, and that `self.handle_parameter` is
registered to handle parameter changes.

We have again provided the implementation of `handle_parameters`, have
a look through it. The callback receives a list of parameters, it goes
through all of them and checks if their name is one we know. If it is
the parameter for the behavior's period, we take and store its value,
which from then will be used in `handle_tick`. The rest of the code is
to report back whether setting the parameter values was
successful. You can use this to disallow invalid values for instance.

Save and rerun; you will see the behavior as before with the head
oscillating with a period of 4 seconds. Now, in another terminal, you
can use the ROS 2 CLI to control this period:

    ros2 param set /sense_act period 2.0

The arguments to `set` here are the node name, the parameter name, and
the desired parameter value.

## Conclusion

Congratulations, you created your first complete robot behavior using
ROS 2!

Here are some suggestions to continue:

* Create another parameter to control the amplitude of the motion
* Try to create some different motion patterns. For instance, have the
  robot look down on the left-to-right pass of the panning pattern,
  and look up on the right-to-left pass.
* Control more joints at the same time. You can list more joints and
  their target values in the `name` and `position` lists of the joint
  command messages. For instance, control `ankle-roll-l` and
  `ankle-roll-r`, together with `hip-roll-l` and `hip-roll-r`, to
  create a left-to-right hip swaying motion.

Of course this is only the beginning, a full robot behavior
stack will have a possibly large number of nodes that interact with
each other through topics. There are several other coordination
patterns that are built on top of these 'send and forget' topics that
you can explore:

- [Services](https://index.ros.org/doc/ros2/Tutorials/Services/Understanding-ROS2-Services/) -
  for when you want a reply to your message. This can for instance be
  used to request the value of a sensor on-demand.
- [Actions](https://index.ros.org/doc/ros2/Tutorials/Understanding-ROS2-Actions/) -
  to deal with long running actions, like triggering a complete
  walking step. By using actions you can get progress updates and
  possible cancel them too.

When you start running more and more nodes together, you want to
combine them in a single setup. This is where [launch
files](https://index.ros.org/doc/ros2/Tutorials/Launch-Files/Creating-Launch-Files/)
come in. In ROS 2 these can be simple text files listing which nodes
to run, or Python files implementing their own complex logic on how to
combine all your nodes.
