# Exercise 2: Advanced ROS 2 features

As we have seen, ROS 2 makes it easy to set up a distributed system of
nodes that can communicate with each other, without having to worry
much about the details of how everything is actually run and how the
communication happens. But on top of that it also provides a range of
advanced features to give you more control over these details. In this
exercise we will look at two of these:

* **Quality of Service** - define trade-offs between reliability and
  robustness to deal with lossy transmission or real-time requirements
* **Node composition** - compose nodes into a single process, with the
  possibility of dynamic loading and unloading of nodes

Let’s get started!

## Copy template package

We will create a ROS 2 package for the nodes demonstrating the
advanced features. Because not all these features are available in
Python, most notably composing Python nodes with other ones, we will
be using C++ here. We have provided a skeleton with the necessary
boilerplate that you can copy over to your workspace’s source
directory. Enter the ADE environment and run the following to get
started:

    # ensure you’re in the home directory
    cd
    mkdir -p src
    cp -r ros2_workshop/exercise_2/ros2_advanced src/
    colcon build --cmake-args " -DCMAKE_BUILD_TYPE=Release" --packages-up-to ros2_advanced
    source install/local_setup.bash

Use the build command above to recompile the package as you edit it
during this exercise. You will not have to source the setup again
unless instructed later (just remember to do it once for every new
terminal inside ADE). To test that the basics work, run the following
nodes in two separate ADE terminals:

    ros2 run image_publisher image_publisher_node ros2_workshop/ros2-workshop-banner.png

and:
    
    ros2 run ros2_advanced image_reader_node

The first node continuously publishes an image on the `/image_raw`
topic, and the second subscribes to this topic and logs some details
about the image.

## Quality of Service

Quality of Service (QoS) profiles let you tweak the communication
between nodes, to enforce or allow reliability levels similar to UDP
(what is used underneath) or for TCP, or for many levels in between,
depending on your requirements. For instance, topics for monitoring
your robot through a lossy WiFi connection can be configured to be
best-effort and volatile since it doesn’t matter if messages get
lost. On the other hand, crucial commands that must not be missed can
be sent with the highest reliability and persistence settings. See the
section about [QoS
policies](https://index.ros.org/doc/ros2/Concepts/About-Quality-of-Service-Settings/#qos-policies)
in the ROS 2 documentation to find out everything that can be
configured. We will get hands-on with tweaking some of these here to
understand their effects.

Open the file `src/ros2_advanced/src/image_reader.cpp` in an
editor. Look through the constructor, where the subscription to the
image topic is set up, as well as the publisher that is used to output
a new image. You will see two QoS profiles that are created: `sub_qos`
and `pub_qos`.

The first is used for subscribing to the `/image_raw` topic and uses
default settings with a queue length of 10. This means the reader
wants reliable communication, will buffer up to 10 messages before
discarding them if they are handled too late, and expects volatile
communication, i.e., it won’t have old messages replayed that were
published from before it subscribed.

The second profile is used for publishing and uses the built in
'sensor data' profile. See again the official documentation for an
[overview of all built in
profiles](https://index.ros.org/doc/ros2/Concepts/About-Quality-of-Service-Settings/#qos-profiles);
the most important thing to know about the sensor data profile is that
it is best-effort.

### Rate and latency

We can inspect the rate and latency of messages published to a topic
using the ROS 2 CLI tools. Enter a new terminal (with the publisher
and reader still running) and run:

    ros2 topic hz /image_raw

You should see an average rate reported towards 10Hz. This may be
lower depending on your machine, because `ros2 topic` can be slow to
read the complete images and can’t keep up; for this exercise that
should be fine. Note the rate, then rerun it for the topic created by
the `image_reader` node:

    ros2 topic hz /workshop/image

You may see that the rate on this topic is (significantly) lower. This
is partly because the (fake) image processing that we are doing takes
too long to keep up with 10Hz: the reader sleeps for 100ms for every
image:

    // Pretend we are doing a lot more work
    std::this_thread::sleep_for(std::chrono::milliseconds(150));

But some of this can also be message loss, which is deemed acceptable
for the best-effort sensor data profile. If you don’t see this effect
strongly, it just means your machine is able to keep up and doesn’t
have to drop messages. The effect of best-effort communication is most
prevalent when communicating over a network.

Where you should definitely see an effect is in message latency. Run
the following to see how long the delay is between the publisher
creating the raw image and the ROS 2 CLI reading it:

    ros2 topic delay /image_raw

This should show a delay of maybe a few dozen milliseconds, which is
again more representative of how efficiently the ROS 2 CLI tools read
the messages. Now try it on the new topic:

    ros2 topic delay /workshop/image

Here you should see a delay of at least a second(!), much more than
you would expect from the processing (sleeping) time. The observant
participant will realise that this delay is about 10x the processing
time, which is exactly the length of the queue we set on the
subscription profile. Try reducing this queue size to zero by
replacing the subscriber QoS profile in
[`image_reader.cpp`](exercise_2/ros2_advanced/src/image_reader.cpp#L21)
with:

    auto sub_qos = rclcpp::QoS{0};

Rebuild, and rerun the reader and measure the delay again. It should
now be close to 200ms. This means the reader is now basically dropping
every second frame, only able to process about 5 frames per second,
which you can test with the `hz` tool.

**Takeaway**: for a slow subscriber, a deeper history/longer queue can
give a higher rate/fewer dropped messages, at the cost of a higher
latency.

"Dropped, you say? I thought we specified that the subscription should
be reliable!”

True, but it is very important to note that the profile ensures
reliable delivery of the message *into a subscriber's input queue*.
After that the responsibility of processing that queue fast enough is
purely with the subscriber.

### QoS profile compatibility

For communication to be set up successfully, the publisher and
subscriber must use compatible profiles. Basically this means that the
profile requested by the subscriber must be no more stringent than
that of the publisher. For instance, reliable communication is more
strict than best-effort, so if the publisher uses the (best-effort)
sensor data profile, a subscription requesting reliable communication
fails. Again we can use the ROS 2 CLI to test this.

With the image publisher and reader running, first try running the
following in a separate ADE terminal:

    ros2 topic echo /workshop/image --qos-reliability best_effort

You should see messages with pixel values fly by. Now try:

    ros2 topic echo /workshop/image --qos-reliability reliable

This doesn’t show anything (not even a warning), because the topic is
published with a less stringent profile than is requested. Try
changing the QoS profile to the default one, which does support
reliable communication:

    auto pub_qos = rclcpp::QoS{10};

And run the two CLI commands again. You now see that both work:
obviously using the same reliability level is fine, and when the
subscriber asks for best effort, the publisher is fine with this less
stringent request, too. Refer to the official documentation again for an
overview of all compatible combinations:
https://index.ros.org/doc/ros2/Concepts/About-Quality-of-Service-Settings/#qos-compatibilities.

**Takeaway**: when creating a publisher, think about what kind of
communication you want to support or guarantee; you could decide to
just use the most stringent policy, allowing the subscriber to make
any choice they want, but ensuring reliable communication comes with
some overhead on both side so you may choose not to support that.

**Before going further**, change both profiles to use
`rclcpp::SensorDataQoS{}` to ensure compatibility across the board.

## Node Composition

By now you may have wondered about the relevance of communication
profiles for nodes running on the same robot. Ideally you just want
messages to be available directly without any possibly interfering
communication layer in between at all. ROS 2 enables you to compose
nodes into a single process without you having to fundamentally change
your code, while supporting nice features like dynamic loading and
unloading of nodes and shared memory communication that circumvents
any copying or further I/O.

Nodes that can be composed in such a way are called ‘components’. You
can see all components that are available in your workspace by
running:

    ros2 component types

You see that `image_publisher` is one of them (unfortunately,
it has some issues as of writing so we won’t use it as a component
here), but our `image_reader` isn’t, yet. It needs to be registered
first. Some steps need to be taken to do this:

1. Add a dependency on the rclcpp_components package. Open
   `src/ros2_advanced/CMakeLists.txt`, find all lines marked `[NODE
   COMPOSITION 1]` (4 in total) and follow the instructions there.
2. Open `src/ros2_advanced/src/image_reader.cpp`, and add the
   following `#include` near the top:

        #include "rclcpp_components/register_node_macro.hpp"

3. In the same file, add the following line as the very last line:

        RCLCPP_COMPONENTS_REGISTER_NODE(ros2_advanced::ImageReader)

Now rebuild the package, and this time do source the workspace again
to make sure that the components are known:

    source install/local_setup.bash
    ros2 component types

You should now see the `ImageReader` component.

## Dynamic Composition

Make sure the image publisher is running. We can now run our nodes as
components in a single container process. First we need to start that
container:

    ros2 run rclcpp_components component_container

This doesn’t output anything yet. To see that it is running properly,
run the following in another terminal:

    ros2 component list

This should output `/ComponentManager`, which is the name of the
container process. Next, load the image reader:

    ros2 component load /ComponentManager ros2_advanced ros2_advanced::ImageReader --node-name image_reader1

This command returns directly, but you will now see the reader’s
output in the terminal where you started the container! What has
happened in the background is that a service call was made to the
container, which loaded the dynamic library that has the image reader
component, and started that component on the fly! Run `ros2 component
list` to see it listed as a component in the container. You can use
the number there (`1` after the first load) to unload the component:

    ros2 component unload /ComponentManager 1

After this, load it again, and you can load a *second* instance of the
reader component:

    ros2 component load /ComponentManager ros2_advanced ros2_advanced::ImageReader --node-name image_reader2 --remap-rule /image_raw:=/workshop/image --remap-rule /workshop/image:=/workshop/image2

This is quite a big command, mostly because we don’t want this new
component to subscribe and publish to the same topics. ROS 2 allows
you to override, or *remap* topic names to fit with the rest of your
framework, as long as the message types (and QoS) are compatible. Here
we remap names so that the name of the input topic is
`/workshop/image`, and the output becomes `/workshop/image2`, i.e. it
subscribes to the output topic of the first reader, and publishes on a
new topic.

You will see the output of the two nodes interleaved in the container
terminal. Running nodes in this way reduces some of the communication
overhead because it all stays in the same process, but it is not
completely free yet: the log messages output the memory addresses at
which messages are stored and if you have a close look you see they
are different for the two nodes. This means that for each message a
complete copy is made, which can be costly for large messages
(e.g. images) and/or very high frequent topics. We can do better than
that!

## Static Composition

Instead of loading components dynamically, you can also compose them
at compile time. Open the file
`src/ros2_advanced/src/composed_readers.cpp` to see what that looks
like: both nodes are created in the same main function, added to an
executor, which is then 'spun' to run both nodes in the same
process. To see how this makes a difference, make sure the image
publisher is still running, and run the composed process with:

    ros2 run ros2_advanced composed_readers

Now when you inspect the output you should see messages like this:

    [INFO] [1593188363.809753999] [image_reader1]: t = 1593188363.640787919 - Publishing image at address: 7fb204001240
    [INFO] [1593188363.809998306] [image_reader2]: t = 1593188363.640787919 - Got image of size: 1920 x 1052 at address: 7fb204001240

showing that the message that the second reader receives is *at the
same memory address* as the message that was originally published by
the first. In other words, communication happens by just passing a
pointer, without making any copies!

It is important to note some requirements for this to fully work:

* Using intra process communication has to be enabled in each node’s
  options, as done in `src/ros2_advanced/src/composed_readers.cpp`.
* The publisher and subscriber should use `unique_ptr`s to send and
  receive messages. This way the ownership semantics of `unique_ptr`
  can be relied upon to guarantee that the publisher has relinquished
  access to the message and it can safely be used by the subscriber.
* If there are multiple subscriptions to the same topic, only one can
  take ownership of the original message. The other(s) still get a
  copy.
