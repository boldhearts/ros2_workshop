cmake_minimum_required(VERSION 3.5)
project(ros2_advanced)

if(NOT CMAKE_C_STANDARD)
  set(CMAKE_C_STANDARD 99)
endif()

if(NOT CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 14)
endif()

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

find_package(ament_cmake REQUIRED)
find_package(rclcpp REQUIRED)
find_package(sensor_msgs REQUIRED)
# [NODE COMPOSITION 1] uncomment the following line
# find_package(rclcpp_components REQUIRED)

add_library(image_reader SHARED
  src/image_reader.cpp)

# [NODE COMPOSITION 1] uncomment the following line
# rclcpp_components_register_nodes(image_reader "ros2_advanced::ImageReader")

target_include_directories(image_reader PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>)

# [NODE COMPOSITION 1] add "rclcpp_components" to the list below
ament_target_dependencies(image_reader
  "rclcpp"
  "sensor_msgs"
)

install(
  TARGETS image_reader
  EXPORT export_${PROJECT_NAME}
  ARCHIVE DESTINATION lib
  LIBRARY DESTINATION lib
  RUNTIME DESTINATION bin
)

add_executable(image_reader_node
  src/image_reader_node.cpp)

target_include_directories(image_reader PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>)

target_link_libraries(image_reader_node image_reader)

install(TARGETS image_reader_node
  EXPORT export_${PROJECT_NAME}
  DESTINATION lib/${PROJECT_NAME})

# [NODE COMPOSITION 1] uncomment the following 12 lines
# add_executable(composed_readers
#   src/composed_readers.cpp)

# target_include_directories(composed_readers PUBLIC
#   $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
#   $<INSTALL_INTERFACE:include>)

# target_link_libraries(composed_readers image_reader)

# install(TARGETS composed_readers
#   EXPORT export_${PROJECT_NAME}
#   DESTINATION lib/${PROJECT_NAME})


if(BUILD_TESTING)
  find_package(ament_lint_auto REQUIRED)
  ament_lint_auto_find_test_dependencies()
endif()

ament_package()
