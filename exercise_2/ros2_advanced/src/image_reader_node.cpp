#include "ros2_advanced/image_reader.hpp"

int main(int argc, char ** argv)
{
  rclcpp::init(argc, argv);

  auto node = std::make_shared<ros2_advanced::ImageReader>(rclcpp::NodeOptions{});

  rclcpp::spin(node);

  return 0;
}
