#include "ros2_advanced/image_reader.hpp"

#include <thread>

namespace ros2_advanced
{

ImageReader::ImageReader(rclcpp::NodeOptions const & options)
: ImageReader::ImageReader(options, "image_reader")
{
}

ImageReader::ImageReader(rclcpp::NodeOptions const & options, std::string name)
: rclcpp::Node{name, options}
{
  // Give the option to set topic names through parameters
  auto input_topic = declare_parameter<std::string>("input_topic", "/image_raw");
  auto output_topic = declare_parameter<std::string>("output_topic", "/workshop/image");

  // Quality of Service profile for subscription
  auto sub_qos = rclcpp::QoS{10};
  // Quality of Service profile for publisher
  auto pub_qos = rclcpp::SensorDataQoS{};

  // Create output image publisher
  image_pub_ = create_publisher<sensor_msgs::msg::Image>(output_topic, pub_qos);

  // Create input image subscription
  image_sub_ = create_subscription<sensor_msgs::msg::Image>(
    input_topic,
    sub_qos,
    [this](std::unique_ptr<sensor_msgs::msg::Image> msg) {
      RCLCPP_INFO_STREAM(
        get_logger(),
        "t = " << msg->header.stamp.sec << "." << msg->header.stamp.nanosec <<
          " - Got image of size: " << msg->width << " x " << msg->height << " at address: " <<
          std::hex << long(msg.get()));

      // Output image is initially a copy of the input
      auto out_msg = std::make_unique<sensor_msgs::msg::Image>();
      *out_msg = *msg;

      // Invert colors
      for (auto & v : out_msg->data) {
        v = 255 - v;
      }
      // Pretend we are doing a lot more work
      std::this_thread::sleep_for(std::chrono::milliseconds(150));

      RCLCPP_INFO_STREAM(
        get_logger(),
        "t = " << msg->header.stamp.sec << "." << msg->header.stamp.nanosec <<
          " - Publishing image at address: " << std::hex << long(out_msg.get()));

      // Republish image onto new topic
      image_pub_->publish(std::move(out_msg));
    });
}

}
