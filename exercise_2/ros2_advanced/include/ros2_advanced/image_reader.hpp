#include <rclcpp/rclcpp.hpp>
#include <sensor_msgs/msg/image.hpp>

namespace ros2_advanced {

    class ImageReader : public rclcpp::Node {

    public:
        ImageReader(rclcpp::NodeOptions const & options);
        ImageReader(rclcpp::NodeOptions const & options, std::string name);
    
    private:
        std::shared_ptr<rclcpp::Subscription<sensor_msgs::msg::Image>> image_sub_;
        std::shared_ptr<rclcpp::Publisher<sensor_msgs::msg::Image>> image_pub_;
    };
}